The main code is now at zack_fast.py - run it like "testgame constanttimeplayer zack_fast.py"

I'm not working on a team, so I've written all the code for this.

Changes since last iteration:

I added some Cython extensions for my board functions that were performance bottlenecks, and that sped my code up a good bit - and I added alpha beta pruning. I think there's a bug somewhere that makes it sometimes make very poor decisions - but I can't for the life of me find it. I determined there was a problem that makes a lot of the decision trees return the same scores, so I tried creating a more varied cost matrix (so that different moves wouldn't have the same cost values) but that didn't seem to help. 


If you want to play with the code, the lines at the top of the zack_fast.py file

iteration_depth = 1
use_minimax = True

These let you configure the settings - iteration depth is how many of your turns forward it will evaluate, plus 1 - so it_depth of 1 evaluates your moves, their responses, your responses, and their responses to that. 1 is fast, 2 is slow enough to be annoying to test. Use_minimax switches on or off the minimax algorithm, in place of a simple evaluator of your current move options (unfortunately due to this bug they both work about the same..).

Dependencies should be the same as last week, Cython isn't required to use the cython module (which is in board_fast.pyx and compiles to board_fast.c if you want to take a peek).
