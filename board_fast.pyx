import numpy as np
cimport numpy as np


DTYPE = np.int32
ctypedef np.int32_t DTYPE_t
cimport cython
@cython.boundscheck(False)



def getMoves(np.ndarray[DTYPE_t, ndim=2] board, int player):

	moves = []
	cdef unsigned int x
	cdef unsigned int y

	for x in range(8):
		for y in range(8):
			if checkMove(board, x, y, player):
				moves.append((x, y))
	return moves


def doMove(np.ndarray[DTYPE_t, ndim=2] board, int x, int y, int player):
	if x != -1 and y != -1:
		result = getMoveResult(board, x, y, player)

		for x, y in result:
			set_position(board, x, y, player)

	

def getMoveResult(np.ndarray[DTYPE_t, ndim=2] board, unsigned int x_in, unsigned int y_in, int player):
	cdef unsigned int X = x_in
	cdef unsigned int Y = y_in
	cdef unsigned int x
	cdef unsigned int y

	cdef int dx
	cdef int dy

	result = []

	# Unnecessary!!!
	#if not checkMove(board, X, Y, player):
	#	raise ValueError("move is not valid!")

	other = 1 if player == 2 else 2
	for dx in xrange(-1, 2):
		for dy in range(-1, 2):
			if (dy == 0 and dx == 0):
				continue

			x = X
			y = Y
			while(True):
				x += dx
				y += dy
				#break if off board or if it's not the other player's piece
				if not((on_board(x, y) and check_position(board, other, x, y))):
					break

			if (on_board(x, y) and check_position(board, player, x, y)):
				x = X
				y = Y
				x += dx
				y += dy
				while (on_board(x, y) and check_position(board, other, x, y)):
					result.append((x, y))
					x += dx
					y += dy

	result.append((X, Y))
	return result

def num_empty_squares(np.ndarray[DTYPE_t, ndim=2] board):
	return (np.equal(np.zeros((8, 8)), board)*np.ones((8, 8))).sum()

cdef unsigned int checkMove(np.ndarray[DTYPE_t, ndim=2] board, unsigned int x, unsigned int y, DTYPE_t player): #35

	#(X, Y) = move
	cdef unsigned int X = x
	cdef unsigned int Y = y
	cdef int dx
	cdef int dy

	if occupied(board, x, y):
		return False

	
	cdef DTYPE_t other = 1 if player == 2 else 2

	for dx in range(-1, 2):
		for dy in range(-1, 2):
			if (dy == 0 and dx == 0):
				continue


			x = X + dx
			y = Y + dy
			if (on_board(x, y) and check_position(board, other, x, y)):
				while True:
					x += dx
					y += dy
					if not ((on_board(x, y) and check_position(board, other, x, y))):
						break

				if (on_board(x, y) and check_position(board, player, x, y)):
					return True
	return False

cdef unsigned int occupied(np.ndarray[DTYPE_t, ndim=2] board, unsigned int x, unsigned int y): #5
	if board[x][y] != 0:
		return True
	else:
		return False

# Checks if the given position is occupied by the given player
cdef unsigned int check_position(np.ndarray[DTYPE_t, ndim=2] board, player, unsigned int x, unsigned int y): #32
	if board[x][y] == player:
		return True
	else:
		return False

cdef int on_board(int x, int y): #20
	return(0 <= x and x < 8 and 0 <= y and y < 8)

cdef void set_position(np.ndarray[DTYPE_t, ndim=2] board, unsigned int x, unsigned int y, DTYPE_t player):
	cdef np.ndarray[DTYPE_t, ndim=2] board2 = board
	board2[x][y] = player
