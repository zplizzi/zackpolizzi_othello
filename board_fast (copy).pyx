import numpy as np
cimport numpy as np

DTYPE = np.int
ctypedef np.int_t DTYPE_t



def getMoves(board, player):
	moves = []
	for x in range(8):
		for y in range(8):
			if checkMove(board, x, y, player):
				moves.append((x, y))
	return moves


def doMove(board, move, player):
	if move[0] != -1 and move[1] != -1:
		result = getMoveResult(board, move, player)

		for x, y in result:
			set_position(board, x, y, player)

	

def getMoveResult(board, move, player): # 21
	X, Y = move
	result = []

	# Unnecessary!!!
	#if not checkMove(board, X, Y, player):
	#	raise ValueError("move is not valid!")

	other = 1 if player == 2 else 2
	for dx in range(-1, 2):
		for dy in range(-1, 2):
			if (dy == 0 and dx == 0):
				continue

			x = X
			y = Y
			while(True):
				x += dx
				y += dy
				#break if off board or if it's not the other player's piece
				if not((on_board(x, y) and check_position(board, other, x, y))):
					break

			if (on_board(x, y) and check_position(board, player, x, y)):
				x = X
				y = Y
				x += dx
				y += dy
				while (on_board(x, y) and check_position(board, other, x, y)):
					result.append((x, y))
					x += dx
					y += dy

	result.append((X, Y))
	return result

def num_empty_squares(board):
	return (np.equal(np.zeros((8, 8)), board)*np.ones((8, 8))).sum()

def checkMove(board, x, y, player): #35

	#(X, Y) = move
	X = x
	Y = y

	if occupied(board, x, y):
		return False

	other = 1 if player == 2 else 2

	for dx in range(-1, 2):
		for dy in range(-1, 2):
			if (dy == 0 and dx == 0):
				continue


			x = X + dx
			y = Y + dy
			if (on_board(x, y) and check_position(board, other, x, y)):
				while True:
					x += dx
					y += dy
				 	if not ((on_board(x, y) and check_position(board, other, x, y))):
				 		break

				if (on_board(x, y) and check_position(board, player, x, y)):
					return True
	return False

def occupied(board, x, y): #5
	if board[x][y] != 0:
		return True
	else:
		return False

# Checks if the given position is occupied by the given player
def check_position(board, player, x, y): #32
	if board[x][y] == player:
		return True
	else:
		return False

def on_board(x, y): #20
	return(0 <= x and x < 8 and 0 <= y and y < 8)

def set_position(board, x, y, player):
	board[x][y] = player