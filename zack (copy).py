#!/usr/bin/python
##!/home/ubuntu/anaconda/bin/python
# Change this for cluster computers!


import sys
import numpy as np
from board import Board
import logging
import os
LOG_FILENAME = 'log.txt'
with open(LOG_FILENAME, 'w'):
    pass
logging.basicConfig(filename=LOG_FILENAME,
	                level=logging.DEBUG,
	                )



costs = np.asarray([[10, -5, 6, 4, 4, 6, -5, 10],
					[-5, -5, 2, 2, 2, 2, -5, -5],
					[6 ,  2, 4, 2, 2, 4,  2,  6],
					[4 ,  2, 2, 2, 2, 2,  2,  4],
					[4 ,  2, 2, 2, 2, 2,  2,  4],
					[6 ,  2, 4, 2, 2, 4,  2,  6],
					[-5, -5, 2, 2, 2, 2, -5, -5],					  
					[10, -5, 6, 4, 4, 6, -5, 10]])


# Board details: 0 is empty, 1 is White, 2 is Black

def init():

	#print moves
	#board.board[5][3] = 5
	#for item in moves:
	#	(x, y) = item
	#	board.board[x][y] = 5
	#print board.board
	print("Init done") # required to signal finish init







def main_loop():
	(y, x, ms_left) = read_input()
	log("their move (and time left):")
	log((x, y, ms_left))

	if x == -1 and y == -1:
		log("other player passed!")
	else:
		board.doMove((x, y), other)
		log("board (after their move):")
		log(board.board)

	
	move = minimax(board)
	log("our move:" + str(move))
	board.doMove(move, player)
	log("Board after our move:")
	log(board.board)
	print move[1], move[0]
	

def no_minimax(board):
	moves = board.getMoves(player)

	if len(moves) > 0:
		values = {}
		for move in moves:
			values[move] = get_cost(board.getMoveResult(move, player))
		log(values)

		best_move = max(values.iterkeys(), key=(lambda key: values[key]))
		return best_move
		
	else:
		return (-1, -1)

def minimax(board):

	original_board = np.copy(board.board)

	moves = board.getMoves(player)

	if len(moves) > 0:


		values = {}
		
		log("Our possible moves: %s" % str(moves))

		for move in moves:
			
			move_scores = []

			board.board = np.copy(original_board)
			board.doMove(move, player)
			current_board = np.copy(board.board)
			their_moves = board.getMoves(other)

			for their_move in their_moves:
				board.board = np.copy(current_board)
				board.doMove(their_move, other)
				move_scores.append(get_board_delta(original_board, board.board, player))
				
			if len(move_scores) > 0:
				values[move] = min(move_scores) 

		board.board = original_board

		log("Our possible moves: %s" % str(values))
		if len(values) > 0:
			best_move = max(values.iterkeys(), key=(lambda key: values[key]))
			return best_move
		else:
			return moves[0]


		
		

	else:
		return (-1, -1)



zeros = np.zeros((8, 8))
ones = np.ones((8, 8))
twos = ones*2

def get_board_delta(board1, board2, player):
	return get_board_cost(board2, player) - get_board_cost(board1, player)

def get_board_cost(board, player):
	our_cost = np.equal(board, ones if player == 1 else twos)*costs
	their_cost = np.equal(board, twos if player == 1 else ones)*costs
	return our_cost.sum() - their_cost.sum()


def get_cost(positions):
	cost = 0
	for position in positions:
		cost += costs[position[0]][position[1]]
	return cost

def log(message):
	logging.debug(message)



def read_input():
	input = raw_input().split(" ")
	if len(input) is not 3:
		writeout("please enter 3 parts!")
	try:
		x = int(input[0])
		y = int(input[1])
		ms_left = int(input[2])
	except:
		writeout("could not parse inputs")

	return (x, y, ms_left)


def writeout(message):
	logging.debug(message)
	sys.stderr.write(message + "\n")
	sys.exit()


if __name__ == "__main__":
	try:
		logging.debug("running init")

		board = Board(init=True)
		sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
	
		player = 1 if sys.argv[1] == "White" else 2
		other = 2 if sys.argv[1] == "White" else 1
		logging.debug("Our color is " + str(sys.argv[1]) + " so " + str(player))

		init()
		logging.debug("init finished")
		while(True):
			logging.debug("running main loop")
			main_loop()
	except:
		logging.exception("Oops:")
