#!/usr/bin/python
##!/home/ubuntu/anaconda/bin/python
# Change this for cluster computers!

#import cProfile
import re
import sys
from board import Board
import numpy as np
import logging
import os



#import pyximport; pyximport.install()

from board_fast import *


LOG_FILENAME = 'log.txt'
with open(LOG_FILENAME, 'w'):
    pass
logging.basicConfig(filename=LOG_FILENAME,
	                level=logging.INFO,
	                )

iteration_depth = 1
test_minimax = False
use_minimax = True

costs = np.ones((8, 8))
if not test_minimax:
	costs = np.asarray([[20, -5, 8, 8, 8, 8, -5, 20],
						[-5, -5, 2, 2, 2, 2, -5, -5],
						[8 ,  2, 4, 2, 2, 4,  2,  8],
						[8 ,  2, 2, 2, 2, 2,  2,  8],
						[8 ,  2, 2, 2, 2, 2,  2,  8],
						[8 ,  2, 4, 2, 2, 4,  2,  8],
						[-5, -5, 2, 2, 2, 2, -5, -5],					  
						[20, -5, 8, 8, 4, 8, -5, 20]])
	"""costs = np.asarray([[148, -10, 48, 49, 50, 51, -10, 160],
						[-10, -11, 18, 16, 17, 19, -11, -10],
						[35 ,  15, 11, 15, 16, 17,  17,  49],
						[37 ,  10, 13, 20, 18, 18,  16,  48],
						[36 ,  11, 12, 19, 21, 14,  16,  35],
						[35 ,  12, 13, 18, 17, 16,  15,  45],
						[-10, -1, 11, 10, 12, 11, -11, -11],					  
						[120, -32, 40, 32, 41, 39, -12, 150]])"""


# Board details: 0 is empty, 1 is White, 2 is Black


def main_loop():
	(y, x, ms_left) = read_input()
	log("their move (and time left):")
	log((x, y, ms_left))

	if x == -1 and y == -1:
		log("other player passed!")
	else:
		doMove(board, x, y, other)
		log("board (after their move):")
		log(board)

	num_empty = num_empty_squares(board)
	log("there are %s empty squares" % str(num_empty))

	if num_empty < 17:
		costs = ones
	
	if use_minimax:
		move = tree(np.copy(board), board, player, iteration_depth, -1000000, 10000000, True)

		if move is not None and move != "too deep":
			(move, dummy) = move
		else:
			if move == "too deep":
				log("reverted back to no minimax")
				move = no_minimax(board)
			else:
				move = (-1, -1)
	else:
		move = no_minimax(board)
	

	log("our move:" + str(move))
	doMove(board, move[0], move[1], player)
	log("Board after our move:")
	log(board)
	print move[1], move[0]
	

def no_minimax(board):
	moves = getMoves(board, player)

	if len(moves) > 0:
		values = {}
		for move in moves:
			values[move] = get_cost(getMoveResult(board, move[0], move[1], player))
		log(values)

		best_move = max(values.iterkeys(), key=(lambda key: values[key]))
		return best_move
		
	else:
		return (-1, -1)




def tree(base_board, board, player, depth, alpha, beta, return_move=False):
	# player is the one who will go in the first step - so always should be me
	# check that other is properly defined
	original_board = board

	

	values_dict = {}

	moves = getMoves(board, player)
	
	if depth >= 1:
		log("original board at depth %s is  \n%s" % (str(depth), str(board)))
		log("our moves: %s" % str(moves))


	if len(moves) > 0:

		for move in moves:
			
			out_scores = []

			board = np.copy(original_board)
			doMove(board, move[0], move[1], player)
			
			current_board = board

			their_moves = getMoves(board, other)

			if depth >= 1:
				log("our move is %s at depth %s" % (move, depth))
				log("board after our move at depth %s is \n%s" % (str(depth), str(board)))
				log("their moves: %s" % str(their_moves))

			
			
			for their_move in their_moves:
				
				board = np.copy(current_board)
				doMove(board, their_move[0], their_move[1], other)


				if depth >= 1:
					log("their move is %s at depth %s" % (their_move, depth))
					log("board after their move at depth %s is \n%s" % (str(depth), str(board)))

				if depth == 0:
					if len(values_dict) == 0 or len(out_scores) == 0:
						delta = get_board_delta(base_board, board, player)
						#log("delta is %s" % str(delta))
						#log("depth is 0")
						out_scores.append(delta)
					elif max(values_dict.values()) < min(out_scores):
						delta = get_board_delta(base_board, board, player)
						#log("delta is %s" % str(delta))
						#log("depth is 0")
						out_scores.append(delta)
					#else:
						#log("didn't compute last layer")
				else:
					result = -500
					#log("values dict lvl %s:" % str(depth) + str(values_dict))
					#log("out_scores dict lvl %s:" % str(depth) + str(out_scores))
					if len(values_dict) == 0 or len(out_scores) == 0:
						#log("one is empty")
						
						for x in range(1, depth+1): # fix things here!
							
							result = tree(base_board, board, player, depth - x, alpha, beta)
							if result != "too deep":
								break
							log("had to re-search less deeply")
							
						if result is not None and result != "too deep":
							if result == -500:
								log("fucked up! fix shit")
							else:
								(dummy, result) = result
								out_scores.append(result)
					elif max(values_dict.values()) < min(out_scores):
						
						for x in range(1, depth+1): # fix things here!
								result = tree(base_board, board, player, depth - x, alpha, beta)
								if result != "too deep":
									break
								log("had to re-search less deeply")
							
						if result is not None and result != "too deep":
							if result == -500:
								log("fucked up! fix shit")
							else:
								(dummy, result) = result
								out_scores.append(result)
					#else:
						#log("did some pruning!")
				
			if len(out_scores) > 0:

				out_scores.sort()
				avg = out_scores[:3]
				values_dict[move] = min(out_scores) #\
					#+ .1 * (sum(avg) / (len(avg)))

		board = original_board

		if len(values_dict) > 0:
			#log("final values dict lvl %s:" % str(depth) + str(values_dict))
			#log("final out_scores dict lvl %s:" % str(depth) + str(out_scores))
			best_move = max(values_dict.iterkeys(), key=(lambda key: values_dict[key]))
			#log("chose the move %s with score %s" % (str(best_move), str(values_dict[best_move])))
			return (best_move, values_dict[best_move])
		else:
			return "too deep"
	else:
		return None
		




# consider what happens when there aren't enough moves left to recurse over

zeros = np.zeros((8, 8))
ones = np.ones((8, 8))
twos = ones*2

def get_board_delta(board1, board2, player):
	#log("board two is:")
	#log(board2)
	return get_board_cost(board2, player) #- get_board_cost(board1, player)

def get_board_cost(board, player):
	our_cost = np.equal(board, ones if player == 1 else twos)
	np.multiply(our_cost, costs, our_cost)
	their_cost = np.equal(board, twos if player == 1 else ones)
	np.multiply(their_cost, costs, their_cost)
	#return matrix_sum(our_cost) - matrix_sum(their_cost)
	return our_cost.sum() - their_cost.sum()

def other_player(player):
	return 1 if player == 2 else 2

def get_cost(positions):
	cost = 0
	for position in positions:
		cost += costs[position[0]][position[1]]
	return cost

def log(message):
	logging.debug(message)



def read_input():
	input = raw_input().split(" ")
	if len(input) is not 3:
		writeout("please enter 3 parts!")
	try:
		x = int(input[0])
		y = int(input[1])
		ms_left = int(input[2])
	except:
		writeout("could not parse inputs")

	return (x, y, ms_left)


def writeout(message):
	logging.debug(message)
	sys.stderr.write(message + "\n")
	sys.exit()











import time


def run2():
	start = time.time()
	for x in range(15):
		logging.debug("running main loop")
		main_loop()
	end = time.time()
	logging.info("LOOP TIME:::" + str(end-start))


def run():
	#cProfile.run("run2()", 'restats')
	run2()
	while(True):
		logging.debug("board data type: " + str(board.dtype))
		main_loop()







board_class = Board(init=True)
board = board_class.board.astype(np.int)

if __name__ == "__main__":
	try:
		logging.debug("running init")
		logging.debug("board data type: " + str(board.dtype))
		sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
	
		player = 1 if sys.argv[1] == "White" else 2
		other = 2 if sys.argv[1] == "White" else 1
		logging.debug("Our color is " + str(sys.argv[1]) + " so " + str(player))

		log("Inital board:")
		log(board)

		logging.debug("init finished")
		print("Init done") # required to signal finish init
		
		run()

	except:
		logging.exception("Oops:")



