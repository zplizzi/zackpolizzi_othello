#!/usr/bin/python
##!/home/ubuntu/anaconda/bin/python
# Change this for cluster computers!

import cProfile
import re
import sys
from board import Board
import numpy as np
import logging
import os
LOG_FILENAME = 'log.txt'
with open(LOG_FILENAME, 'w'):
    pass
logging.basicConfig(filename=LOG_FILENAME,
	                level=logging.DEBUG,
	                )

iteration_depth = 1
test_minimax = False

costs = np.ones((8, 8))
if not test_minimax:
	costs = np.asarray([[10, -5, 6, 4, 4, 6, -5, 10],
						[-5, -5, 2, 2, 2, 2, -5, -5],
						[6 ,  2, 4, 2, 2, 4,  2,  6],
						[4 ,  2, 2, 2, 2, 2,  2,  4],
						[4 ,  2, 2, 2, 2, 2,  2,  4],
						[6 ,  2, 4, 2, 2, 4,  2,  6],
						[-5, -5, 2, 2, 2, 2, -5, -5],					  
						[10, -5, 6, 4, 4, 6, -5, 10]])
	"""costs = np.asarray([[10, -5, 40, 40, 40, 40, -5, 10],
						[-5, -5, 10, 10, 10, 10, -5, -5],
						[8 ,  4, 4, 2, 2, 4,  10,  40],
						[8 ,  4, 2, 2, 2, 2,  10,  40],
						[8 ,  4, 2, 2, 2, 2,  10,  40],
						[8 ,  4, 4, 2, 2, 4,  10,  40],
						[-5, -5, 4, 4, 4, 4, -5, -5],					  
						[10, -5, 8, 8, 8, 8, -5, 10]])"""


# Board details: 0 is empty, 1 is White, 2 is Black


def main_loop():
	(y, x, ms_left) = read_input()
	log("their move (and time left):")
	log((x, y, ms_left))

	if x == -1 and y == -1:
		log("other player passed!")
	else:
		doMove(board, (x, y), other)
		log("board (after their move):")
		log(board)

	num_empty = num_empty_squares(board)
	log("there are %s empty squares" % str(num_empty))

	if num_empty < 17:
		costs = ones
	
	move = tree(np.copy(board), board, player, iteration_depth, True)

	if move == "too deep":
		log("reverted back to no minimax")
		move = no_minimax(board)

	log("our move:" + str(move))
	doMove(board, move, player)
	log("Board after our move:")
	log(board)
	print move[1], move[0]
	

def no_minimax(board):
	moves = getMoves(board, player)

	if len(moves) > 0:
		values = {}
		for move in moves:
			values[move] = get_cost(getMoveResult(board, move, player))
		log(values)

		best_move = max(values.iterkeys(), key=(lambda key: values[key]))
		return best_move
		
	else:
		return (-1, -1)




def tree(base_board, board, player, depth, return_move=False):
	# player is the one who will go in the first step - so always should be me
	# check that other is properly defined
	original_board = board

	
	values = []
	values_dict = {}

	moves = getMoves(board, player)
	#log("moves: %s" % str(moves))

	if len(moves) > 0:

		for move in moves:
			out_scores = []

			board = np.copy(original_board)
			doMove(board, move, player)
			current_board = board
			their_moves = getMoves(board, other)
			#log("their moves: %s" % str(their_moves))
			for their_move in their_moves:
				board = np.copy(current_board)
				doMove(board, their_move, other)
				if depth == 0:
					delta = get_board_delta(base_board, board, player)
					#log("delta is %s" % str(delta))
					out_scores.append(delta)
				else:
					result = -500
					for x in range(1, depth+1): # fix things here!
						result = tree(base_board, board, player, depth - x)
						if result != "too deep":
							break
						log("had to re-search less deeply")
					if result == -500:
						log("fucked up! fix shit")
					if result is not None:
						out_scores.append(result)
					
				
			if len(out_scores) > 0:
				if not return_move:
					values.append(min(out_scores))
				else:
					values_dict[move] = min(out_scores)

		board = original_board

		if len(values) > 0 or len(values_dict) > 0:
			if not return_move:
				return max(values)
			else:
				#log(values_dict)
				return max(values_dict.iterkeys(), key=(lambda key: values_dict[key]))
		else:
			return "too deep"
	else:
		if not return_move:
			return None
		else:
			return (-1, -1)




# consider what happens when there aren't enough moves left to recurse over

zeros = np.zeros((8, 8))
ones = np.ones((8, 8))
twos = ones*2

def get_board_delta(board1, board2, player):
	#log("board two is:")
	#log(board2)
	return get_board_cost(board2, player) - get_board_cost(board1, player)

def get_board_cost(board, player):
	our_cost = np.equal(board, ones if player == 1 else twos)*costs
	their_cost = np.equal(board, twos if player == 1 else ones)*costs
	return our_cost.sum() - their_cost.sum()

def other_player(player):
	return 1 if player == 2 else 2

def get_cost(positions):
	cost = 0
	for position in positions:
		cost += costs[position[0]][position[1]]
	return cost

def log(message):
	logging.debug(message)



def read_input():
	input = raw_input().split(" ")
	if len(input) is not 3:
		writeout("please enter 3 parts!")
	try:
		x = int(input[0])
		y = int(input[1])
		ms_left = int(input[2])
	except:
		writeout("could not parse inputs")

	return (x, y, ms_left)


def writeout(message):
	logging.debug(message)
	sys.stderr.write(message + "\n")
	sys.exit()







def getMoves(board, player):
	moves = []
	for x in range(8):
		for y in range(8):
			if checkMove(board, x, y, player):
				moves.append((x, y))
	return moves


def doMove(board, move, player):
	if move[0] != -1 and move[1] != -1:
		result = getMoveResult(board, move, player)

		for x, y in result:
			set_position(board, x, y, player)

	

def getMoveResult(board, move, player): # 21
	X, Y = move
	result = []

	# Unnecessary!!!
	if not checkMove(board, X, Y, player):
		raise ValueError("move is not valid!")

	other = 1 if player == 2 else 2
	for dx in range(-1, 2):
		for dy in range(-1, 2):
			if (dy == 0 and dx == 0):
				continue

			x = X
			y = Y
			while(True):
				x += dx
				y += dy
				if not((on_board(board, x, y) and check_position(board, other, x, y))):
					break

			if (on_board(board, x, y) and check_position(board, player, x, y)):
				x = X
				y = Y
				x += dx
				y += dy
				while (on_board(board, x, y) and check_position(board, other, x, y)):
					result.append((x, y))
					x += dx
					y += dy

	result.append((X, Y))
	return result

def num_empty_squares(board):
	return (np.equal(np.zeros((8, 8)), board)*np.ones((8, 8))).sum()

def checkMove(board, x, y, player): #35

	#(X, Y) = move
	X = x
	Y = y

	if occupied(board, x, y):
		return False

	other = 1 if player == 2 else 2

	for dx in range(-1, 2):
		for dy in range(-1, 2):
			if (dy == 0 and dx == 0):
				continue


			x = X + dx
			y = Y + dy
			if (on_board(board, x, y) and check_position(board, other, x, y)):
				while True:
					x += dx
					y += dy
				 	if not ((on_board(board, x, y) and check_position(board, other, x, y))):
				 		break

				if (on_board(board, x, y) and check_position(board, player, x, y)):
					return True
	return False

def occupied(board, x, y): #5
	if board[x][y] != 0:
		return True
	else:
		return False

# Checks if the given position is occupied by the given player
def check_position(board, player, x, y): #32
	if board[x][y] == player:
		return True
	else:
		return False

def on_board(board, x, y): #20
	return(0 <= x and x < 8 and 0 <= y and y < 8)

def set_position(board, x, y, player):
	board[x][y] = player








def run2():

	for x in range(7):
		logging.debug("running main loop")
		main_loop()


def run():
	#cProfile.run("run2()", 'restats')

	while(True):
		
		main_loop()






board_class = Board(init=True)
board = board_class.board

if __name__ == "__main__":
	try:
		logging.debug("running init")

		sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
	
		player = 1 if sys.argv[1] == "White" else 2
		other = 2 if sys.argv[1] == "White" else 1
		logging.debug("Our color is " + str(sys.argv[1]) + " so " + str(player))

		log("Inital board:")
		log(board)

		logging.debug("init finished")
		print("Init done") # required to signal finish init
		
		run()

	except:
		logging.exception("Oops:")



