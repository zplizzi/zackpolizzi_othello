import numpy as np

class Board:

	def __init__(self, init=False, board=None):
		
		if board is None:
			self.board = np.zeros((8, 8))
			
			if init:
				self.board[3][3] = 1
				self.board[4][3] = 2
				self.board[3][4] = 2
				self.board[4][4] = 1
		else:
			self.board = board

	def getMoves(self, player):
		moves = []
		for x in range(8):
			for y in range(8):
				if self.checkMove(x, y, player):
					moves.append((x, y))
		return moves


	def doMove(self, move, player):
		if move[0] != -1 and move[1] != -1:
			result = self.getMoveResult(move, player)

			for x, y in result:
				self.set_position(x, y, player)

		

	def getMoveResult(self, move, player): # 21
		X, Y = move
		result = []
		if not self.checkMove(X, Y, player):
			raise ValueError("move is not valid!")

		other = 1 if player == 2 else 2
		for dx in range(-1, 2):
			for dy in range(-1, 2):
				if (dy == 0 and dx == 0):
					continue

				x = X
				y = Y
				while(True):
					x += dx
					y += dy
					if not((self.on_board(x, y) and self.check_position(other, x, y))):
						break

				if (self.on_board(x, y) and self.check_position(player, x, y)):
					x = X
					y = Y
					x += dx
					y += dy
					while (self.on_board(x, y) and self.check_position(other, x, y)):
						result.append((x, y))
						x += dx
						y += dy

		result.append((X, Y))
		return result

	def num_empty_squares(self):
		return (np.equal(np.zeros((8, 8)), self.board)*np.ones((8, 8))).sum()

	def checkMove(self, x, y, player): #35

		#(X, Y) = move
		X = x
		Y = y

		if self.occupied(x, y):
			return False

		other = 1 if player == 2 else 2

		for dx in range(-1, 2):
			for dy in range(-1, 2):
				if (dy == 0 and dx == 0):
					continue


				x = X + dx
				y = Y + dy
				if (self.on_board(x, y) and self.check_position(other, x, y)):
					while True:
						x += dx
						y += dy
					 	if not ((self.on_board(x, y) and self.check_position(other, x, y))):
					 		break

					if (self.on_board(x, y) and self.check_position(player, x, y)):
						return True
		return False

	def occupied(self, x, y): #5
		if self.board[x][y] != 0:
			return True
		else:
			return False

	def check_position(self, player, x, y): #32
		if self.board[x][y] == player:
			return True
		else:
			return False

	def on_board(self, x, y): #20
		return(0 <= x and x < 8 and 0 <= y and y < 8)

	def set_position(self, x, y, player):
		self.board[x][y] = player


